#!/bin/bash

if ! command -v wget &> /dev/null
then
    echo "Installing wget"
    sudo apt install -y wget
fi

if ! command -v xelatex &> /dev/null
then
    echo "Installing xelatex"
    sudo apt install -y texlive-xetex
fi

if ! command -v pandoc &> /dev/null
then
    echo "Installing pandoc"
    wget https://github.com/jgm/pandoc/releases/download/2.16.2/pandoc-2.16.2-1-amd64.deb
    sudo dpkg -i pandoc*.deb
fi

echo "Done"
