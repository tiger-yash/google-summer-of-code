<!--
.. title: Create a WebUI for data mapping and software control in embedded systems
.. slug: idea-embedded-system-webui
.. author: Edu Meneses
.. date: 2022-03-07 09:35:11 UTC-05:00
.. tags: ideas, medium, 175 hours, SATIE, LivePose
.. type: text
-->

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resources:
https://google.github.io/gsocguides/mentor/defining-a-project-ideas-list
https://google.github.io/gsocguides/mentor/making-your-ideas-page
-->

<!-- 
Please update post metadata: 
* title
* slug (the unique string used in URL, dash-separated list of lowercase words, starting with the idea-, identical as filename)
* author (comma-separated list of mentors)
* date (not in future otherwise not visible now when the website is built)
* tags (comma-separated, starting with: ideas, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

## Project title/description

<!-- Please add the title below this comment to match the value in post title metadata -->
Create a WebUI for data mapping and software control in embedded systems

## More detailed description of the project

<!-- Please write 2-5 sentences below this comment -->
[SAT tools](https://sat.qc.ca/fr/recherche/logiciels) have been used by artists and in both artistic residencies and independent new-media art projects.
The Metalab team constantly works to make the deployment, integration, and use of these tools easier for artists.
This project consists in creating a WebUI for remotely launching (and possibly integrating and mapping) SAT tools in embedded systems based on the [Raspberry Pi](https://www.raspberrypi.org/) and, in some use cases, [NVIDIA Jetson Edge devices](https://www.nvidia.com/en-us/autonomous-machines/embedded-systems/).

More specifically, you GSoC contributor will help build a WebUI that acts as an entry point for SAT Metalab tools. The first goal for the WebUI is to enable their users to choose some basic config settings and launch a set of tools (e.g., [SATIE](https://sat-metalab.gitlab.io/en/) or [LivePose](https://sat-metalab.gitlab.io/en/)) from the WebUI.

The reference tools (from previous projects) include [Poire](https://gitlab.com/sat-metalab/poire) and [Telluriq](https://gitlab.com/sat-mtl/telepresence/telluriq).
Poire is a prototype web controller for SATIE while Telluriq is a graphical user interface that monitors and calibrates haptic floors.

The applications include setup, maintenance, and control of art installations, sound spatialization, and music performances.
[Mirador](https://gitlab.com/sat-metalab/hardware/mirador/) (to be renamed) is one possible hardware module where the WebUI could be deployed.
[React](https://reactjs.org) and Javascript are one possible set of framework and programming language to be used in the project.
However, the mentors are open to discussing with the applicant the technologies used in the project.
The existing code from Poire and Telluriq may or may not be used in this project, according to the programming language that we will choose and according to the contributor's assessment.

## Expected outcomes

<!-- Please add 2-5 items below this comment -->

- WebUI capable of launching and control basic functionality of the following software:
    - [SATIE](https://sat-metalab.gitlab.io/en/)
    - [LivePose](https://sat-metalab.gitlab.io/en/)

## Skills required/preferred

<!-- Please add 2-5 items below this comment -->

- required: basic knowledge of networking protocols, specially OpenSoundControl (OSC)
- required: passing knowledge of or willingness to learn Python and C++ for adaptations of the embedding backend
- preferred: willingness to learn NodeJS and JavaScript for the frontend (there is some flexibility on the choice of the WebUI frontend language)

## Possible mentors

<!-- Please list yourself/yourselves. 2 possible mentors are more failsafe. -->
[Edu Meneses](https://gitlab.com/edumeneses), [Christian Frisson](https://gitlab.com/christianfrisson), [Valentin Laurent](https://gitlab.com/vlaurent)

## Expected size of the project

<!-- Please write below this comment either: 175 hours or 350 hours -->

175 hours

## Rating of difficulty

<!-- Please write below this comment either: easy, medium or hard -->

medium
